import Navigator from "./components/Navigator";
import Footer from "./components/Footer";
import Header from "./components/Header";
import AboutUs from "./components/AboutUs";
import Services from "./components/Services";

function App() {
  return (
    <>
      <Navigator />
      <Header />
      <AboutUs />
      <Services />
      <Footer />
    </>
  );
}

export default App;
