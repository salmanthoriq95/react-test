import header_img from '../header_img.png'

const Header = () => {
  return (
    <div className="bg-primary">           
        <div class="container text-center">
            <div class="row align-items-start">
                <div class="col pt-5 mt-5">
                    <h2 className='text-white text-start pt-2 ps-5'>The <span className="h1 text-warning">BEST</span> One Page Web Template</h2>
                    <p className='text-white pt-2 ps-5 text-start'>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the galley of type and scrambled it to make a type specimen.</p>
                    <div className="d-grid gap-2 d-md-flex ps-5 pt-2 justify-content-md-start">
                        <button type="button" class="btn btn-outline-light btn-lg rounded-0">Primary button</button>
                    </div>
                </div>
                <div class="col">
                    <div className="container text-end">
                        <img className="img-fluid" src={header_img}></img>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Header
