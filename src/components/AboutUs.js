import about_img from '../about-img.jpeg'

const AboutUs = () => {
  return (
    <div className="bg-white" id='aboutus'>         
        <h1 className='text-center p-3 fw-bold'>ABOUT US</h1>  
        <div class="container text-center">
            <div class="row align-items-start">
                <div class="col pt-5 mt-5 align-items-start">
                    <p className='text-start fw-bold fs-5'>Lorem Ipsum has been the industry's standard dummy text ever..</p>
                    <p className='fw-light text-start text-muted'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.PageMaker including versions of Lorem Ipsum. </p>
                    <p className='fw-light text-start text-muted'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div>
                <div class="col">
                    <div className="container text-end p-5">
                        <img className="img-fluid" src={about_img}></img>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default AboutUs
