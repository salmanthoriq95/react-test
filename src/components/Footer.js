const Footer = () => {
  return (
    <div class="text-center bg-dark pt-3 footer mb-0">
        <h1 className="p-3 m-3 text-white">Contact Us</h1>
        <div class="row align-items-start">
            <div class="col ps-5 ms-5">
                <div className="text-start ps-5">
                    <h3 className="text-white-50">Ballet One Page</h3>
                    <p className="text-white">104, Some street, NewYork, USA</p>
                    <h3 className="text-white-50">call us</h3>
                    <p className="text-white">+1 234 567890</p>
                    <h3 className="text-white-50">Email us</h3>
                    <p className="text-white">support@sitename.com</p>
                </div>
            </div>
            <div class="col">
                <form className="me-3 p-3">
                    <input type="text" className="form-control mb-3 rounded-0" id="fullName" placeholder="Full Name" />
                    <input type="email" className="form-control mb-3 rounded-0" id="email" placeholder="Email" />
                    <textarea className="form-control mb-3 rounded-0" id="msg" rows="3" placeholder="Message"></textarea>
                    <div className="float-end p-3">
                        <button type="button" className="btn btn-primary btn-lg">Send</button>
                    </div>
                </form>
            </div>
        </div>

        <div>
            <hr class="border-white mb-0" />
            <p className="text-white p-3">Copyright © 2017, Template by WebThemez.com</p>
        </div>
        
    </div>
  )
}

export default Footer
