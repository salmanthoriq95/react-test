import logo from '../logo.png'

const Navigator = () => {
  return (
    <nav className="navbar navbar-expand-lg bg-light">
        <div className="container-fluid me-5 ms-5">
            <a className="navbar-brand" href="#"><img src={logo}/></a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse flex-row-reverse p-3" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item ms-5">
                    <a className="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li className="nav-item ms-5">
                    <a className="nav-link" href="#aboutus">About Us</a>
                    </li>
                    <li className="nav-item ms-5">
                    <a className="nav-link" href="#services">Services</a>
                    </li>
                </ul>
            </div>
        </div>  
    </nav>
  )
}

export default Navigator
