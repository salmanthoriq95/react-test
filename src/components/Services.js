import {AiFillApple, AiFillAndroid, AiFillHtml5, AiOutlineDropbox} from "react-icons/ai"
import {FaSlackHash, FaUsers} from 'react-icons/fa'

const Services = () => {
  return (
    <div className="bg-light" id="services">
        <h1 className="text-center p-3 fw-bold">SERVICES</h1>
        <div class="container text-center">
            <div class="row align-items-start">
                <div class="pt-4 bg-primary col rounded-top me-4 mb-4 ms-4 p-0">
                    <div className="bg-info mt-3 border border-3 border-bottom-0 border-start-0 border-end-0 border-bottom-0 rounded-4 rounded-bottom border-dashed p-4">
                        <AiFillAndroid size={50} color={'white'}/>
                    </div>
                    <p className="mt-3 p-2 fs-4 fw-bold text-warning">ANDROID</p>
                    <p className="p-2 m-4 pb-5 text-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                </div>
                <div class="pt-4 bg-primary col rounded-top me-4 mb-4 p-0">
                    <div className="bg-info mt-3 border border-3 border-bottom-0 border-start-0 border-end-0 border-bottom-0 rounded-4 rounded-bottom border-dashed p-4">
                        <AiFillApple size={50} color={'white'}/>
                    </div>
                    <p className="mt-3 p-2 fs-4 fw-bold text-warning">APPLE</p>
                    <p className="p-2 m-4 pb-5 text-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                </div>
                <div class="pt-4 bg-primary col rounded-top me-4 mb-4 p-0">
                    <div className="bg-info mt-3 border border-3 border-bottom-0 border-start-0 border-end-0 border-bottom-0 rounded-4 rounded-bottom border-dashed p-4">
                        <AiFillHtml5 size={50} color={'white'}/>
                    </div>
                    <p className="mt-3 p-2 fs-4 fw-bold text-warning">DESIGN</p>
                    <p className="p-2 m-4 pb-5 text-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                </div>
            </div>
            <div class="row align-items-end">
                <div class="pt-4 bg-primary col rounded-top me-4 mb-4 ms-4 p-0">
                    <div className="bg-info mt-3 border border-3 border-bottom-0 border-start-0 border-end-0 border-bottom-0 rounded-4 rounded-bottom border-dashed p-4">
                        <AiOutlineDropbox size={50} color={'white'}/>
                    </div>
                    <p className="mt-3 p-2 fs-4 fw-bold text-warning">CONCEPT</p>
                    <p className="p-2 m-4 pb-5 text-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                </div>
                <div class="pt-4 bg-primary col rounded-top me-4 mb-4 p-0">
                    <div className="bg-info mt-3 border border-3 border-bottom-0 border-start-0 border-end-0 border-bottom-0 rounded-4 rounded-bottom border-dashed p-4">
                        <FaSlackHash size={50} color={'white'}/>
                    </div>
                    <p className="mt-3 p-2 fs-4 fw-bold text-warning">USER RESEARCH</p>
                    <p className="p-2 m-4 pb-5 text-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                </div>
                <div class="pt-4 bg-primary col rounded-top me-4 mb-4 p-0">
                    <div className="bg-info mt-3 border border-3 border-bottom-0 border-start-0 border-end-0 border-bottom-0 rounded-4 rounded-bottom border-dashed p-4">
                        <FaUsers size={50} color={'white'}/>
                    </div>
                    <p className="mt-3 p-2 fs-4 fw-bold text-warning">USER EXPERIENCE</p>
                    <p className="p-2 m-4 pb-5 text-white">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Services
